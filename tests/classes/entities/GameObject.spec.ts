import GameObject from '@/classes/entities/GameObject';
import { GameObjectProps } from '@/ts/classes';

const setGameObject = (props: Partial<GameObjectProps>) => {
  const canvas = document.createElement('canvas');
  const ctx = canvas.getContext('2d') as CanvasRenderingContext2D;
  props.posX = props.posX || 0;
  props.posY = props.posY || 0;
  const gameObject = new GameObject({ canvas, ctx, ...props });

  return { canvas, ctx, gameObject };
};

describe('GameObject', () => {
  it('should have a top margin position equal to the y-axis value', () => {
    const { gameObject } = setGameObject({ posY: 10 });
    expect(gameObject.topMarginPosition).toEqual(gameObject.position.y);
  });

  it("should have a right margin position equal to the x-axis plus the object's width", () => {
    const { gameObject } = setGameObject({ posX: 10, width: 10 });
    expect(gameObject.rightMarginPosition).toEqual(gameObject.position.x + 10);
  });

  it("should have a bottom margin position equal to the y-axis plus the object's height", () => {
    const { gameObject } = setGameObject({ posY: 10, height: 10 });
    expect(gameObject.bottomMarginPosition).toEqual(gameObject.position.y + 10);
  });

  it('should have a left margin position equal to the x-axis', () => {
    const { gameObject } = setGameObject({ posX: 10 });
    expect(gameObject.leftMarginPosition).toEqual(gameObject.position.x);
  });

  it("should touch above another object when its top margin is equal to the other object's bottom margin", () => {
    const { gameObject } = setGameObject({ posY: 110 });
    const { gameObject: anotherGameObject } = setGameObject({
      posY: 100,
      height: 10,
    });

    const isTopMarginAdjacent = gameObject.touchesAbove(anotherGameObject);

    expect(isTopMarginAdjacent).toEqual(true);
  });

  it("should touch below another object when its bottom margin is equal to the other object's top margin", () => {
    const { gameObject } = setGameObject({ posY: 110, height: 10 });
    const { gameObject: anotherGameObject } = setGameObject({
      posY: 120,
    });

    const isTopMarginAdjacent = gameObject.touchesBelow(anotherGameObject);

    expect(isTopMarginAdjacent).toEqual(true);
  });

  it("should touch on the right another object when its right margin is equal to the other object's left margin", () => {
    const { gameObject: object1 } = setGameObject({ posX: 100, width: 10 });
    const { gameObject: object2 } = setGameObject({
      posX: 110,
    });

    expect(object1.touchesOnTheRight(object2)).toEqual(true);
  });

  it("should touch on the left another object when its left margin is equal to the other object's right margin", () => {
    const { gameObject: object1 } = setGameObject({
      posX: 110,
    });
    const { gameObject: object2 } = setGameObject({ posX: 100, width: 10 });

    expect(object1.touchesOnTheLeft(object2)).toEqual(true);
  });

  it('should draw a rectangle', function () {
    const { gameObject, ctx } = setGameObject({});
    const fillRect = jest.spyOn(ctx, 'fillRect');

    gameObject.draw();

    expect(fillRect).toBeCalledWith(0, 0, 30, 30);
  });

  it('should draw a rectangle of given size and position', function () {
    const { gameObject, ctx } = setGameObject({
      posY: 100,
      posX: 100,
      width: 10,
      height: 10,
    });

    const fillRect = jest.spyOn(ctx, 'fillRect');

    gameObject.draw();

    expect(fillRect).toBeCalledWith(100, 100, 10, 10);
  });
});
