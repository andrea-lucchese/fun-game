import MovingGameObject from '@/classes/entities/MovingGameObject';
import { MovementModifier } from '@/classes/modifiers/MovementModifier';
import { gravity } from '@/libs/constants/gravity';

import { setCanvasAndContext } from '../../helpers';
import Player from '@/classes/entities/Player';

//#region Tests Helpers
class MovingGameObjectUnprotected extends MovingGameObject {
  public updatePosition = () => {
    return super.updatePosition();
  };
  public moveHorizontally = () => {
    return super.moveHorizontally();
  };
}

const movingObjectBaseProps = setCanvasAndContext();

const resetMovingObjectBaseProps = () => {
  const { canvas, ctx } = setCanvasAndContext();
  movingObjectBaseProps.canvas = canvas;
  movingObjectBaseProps.ctx = ctx;
};
//#endregion Tests Helpers

describe('MovingGameObject', () => {
  beforeEach(() => {
    resetMovingObjectBaseProps();
  });

  describe('acceleration', () => {
    it('should increase x-axis velocity by its acceleration capability when accelerating right', () => {
      const movingGameObject = new MovingGameObject({
        ...movingObjectBaseProps,
        velocity: { x: 0, y: 0 },
        acceleration: 2,
        maxSpeed: 10,
      });

      movingGameObject.accelerateRight();

      expect(movingGameObject.velocity.x).toEqual(2);

      movingGameObject.accelerateRight();

      expect(movingGameObject.velocity.x).toEqual(4);
    });

    it('should not be able to accelerate right when the maximum speed is reached', () => {
      const movingGameObject = new MovingGameObject({
        ...movingObjectBaseProps,
        velocity: { x: 0, y: 0 },
        acceleration: 2,
        maxSpeed: 2,
      });

      movingGameObject.accelerateRight();

      expect(movingGameObject.velocity.x).toEqual(2);

      movingGameObject.accelerateRight();

      expect(movingGameObject.velocity.x).toEqual(2);
    });

    it('should decrease x-axis velocity by its acceleration capability when decelerating right', () => {
      const movingGameObject = new MovingGameObject({
        ...movingObjectBaseProps,
        velocity: { x: 6, y: 0 },
        acceleration: 2,
        maxSpeed: 10,
      });

      movingGameObject.decelerateRight();

      expect(movingGameObject.velocity.x).toEqual(4);

      movingGameObject.decelerateRight();

      expect(movingGameObject.velocity.x).toEqual(2);
    });

    it('should not be able to decelerate right when the x-axis velocity is zero', () => {
      const movingGameObject = new MovingGameObject({
        ...movingObjectBaseProps,
        velocity: { x: 0, y: 0 },
        acceleration: 2,
        maxSpeed: 10,
      });

      movingGameObject.decelerateRight();

      expect(movingGameObject.velocity.x).toEqual(0);

      movingGameObject.decelerateRight();

      expect(movingGameObject.velocity.x).toEqual(0);
    });

    it('should decrease x-axis velocity its acceleration capability when accelerating left', () => {
      const movingGameObject = new MovingGameObject({
        ...movingObjectBaseProps,
        velocity: { x: 0, y: 0 },
        acceleration: 2,
        maxSpeed: 10,
      });

      movingGameObject.accelerateLeft();

      expect(movingGameObject.velocity.x).toEqual(-2);

      movingGameObject.accelerateLeft();

      expect(movingGameObject.velocity.x).toEqual(-4);
    });

    it('should not be able to accelerate left when negative maximum speed is reached', () => {
      const movingGameObject = new MovingGameObject({
        ...movingObjectBaseProps,
        velocity: { x: 0, y: 0 },
        acceleration: 2,
        maxSpeed: 2,
      });

      movingGameObject.accelerateLeft();

      expect(movingGameObject.velocity.x).toEqual(-2);

      movingGameObject.accelerateLeft();

      expect(movingGameObject.velocity.x).toEqual(-2);
    });

    it('should increase x-axis velocity by its acceleration capability when decelerating left', () => {
      const movingGameObject = new MovingGameObject({
        ...movingObjectBaseProps,
        velocity: { x: -6, y: 0 },
        acceleration: 2,
        maxSpeed: 10,
      });

      movingGameObject.decelerateLeft();

      expect(movingGameObject.velocity.x).toEqual(-4);

      movingGameObject.decelerateLeft();

      expect(movingGameObject.velocity.x).toEqual(-2);
    });

    it('should not be able to decelerate left when the x-axis velocity is zero', () => {
      const movingGameObject = new MovingGameObject({
        ...movingObjectBaseProps,
        velocity: { x: -2, y: 0 },
        acceleration: 2,
        maxSpeed: 10,
      });

      movingGameObject.decelerateLeft();

      expect(movingGameObject.velocity.x).toEqual(0);

      movingGameObject.decelerateLeft();

      expect(movingGameObject.velocity.x).toEqual(0);
    });
  });

  describe('velocity', () => {
    it('should increase X velocity by acceleration when moving RIGHt', () => {
      const movingGameObject = new MovingGameObject({
        ...movingObjectBaseProps,
      });
      const originalVelocity = movingGameObject.velocity.x;

      movingGameObject.accelerateRight();

      expect(movingGameObject.velocity.x).toEqual(
        originalVelocity + movingGameObject.acceleration,
      );
    });

    it('should not exceed maximum speed when moving RIGHT', () => {
      const movingGameObject = new MovingGameObject({
        ...movingObjectBaseProps,
      });
      movingGameObject.maxSpeed = 6;
      movingGameObject.acceleration = 3;

      movingGameObject.accelerateRight();

      expect(movingGameObject.velocity.x).toEqual(
        movingGameObject.acceleration,
      );

      movingGameObject.accelerateRight();

      expect(movingGameObject.velocity.x).toEqual(
        movingGameObject.acceleration * 2,
      );

      movingGameObject.accelerateRight();

      expect(movingGameObject.velocity.x).toEqual(
        movingGameObject.acceleration * 2,
      );
    });

    it('should decrease X velocity by acceleration when moving LEFT', () => {
      const movingGameObject = new MovingGameObject({
        ...movingObjectBaseProps,
      });
      const originalVelocity = movingGameObject.velocity.x;

      movingGameObject.accelerateLeft();

      expect(movingGameObject.velocity.x).toEqual(
        originalVelocity - movingGameObject.acceleration,
      );
    });

    it('should not exceed maximum speed when moving LEFT', () => {
      const movingGameObject = new MovingGameObject({
        ...movingObjectBaseProps,
      });
      movingGameObject.maxSpeed = 6;
      movingGameObject.acceleration = 3;

      movingGameObject.accelerateLeft();

      expect(movingGameObject.velocity.x).toEqual(
        -movingGameObject.acceleration,
      );

      movingGameObject.accelerateLeft();

      expect(movingGameObject.velocity.x).toEqual(
        -(movingGameObject.acceleration * 2),
      );

      movingGameObject.accelerateLeft();

      expect(movingGameObject.velocity.x).toEqual(
        -(movingGameObject.acceleration * 2),
      );
    });
  });

  describe('when updated', () => {
    it('should draw a rectangle', () => {
      const movingGameObject = new MovingGameObject({
        ...movingObjectBaseProps,
      });

      const draw = jest.spyOn(movingGameObject, 'draw');

      movingGameObject.update();

      expect(draw).toBeCalled();
    });

    it('should update position', () => {
      const movingGameObject = new MovingGameObject({
        ...movingObjectBaseProps,
      });

      const updatePosition = jest.spyOn(
        movingGameObject as MovingGameObjectUnprotected,
        'updatePosition',
      );

      movingGameObject.update();

      expect(updatePosition).toBeCalled();
    });

    it('should handle jumps', () => {
      const movingGameObject = new MovingGameObject({
        ...movingObjectBaseProps,
      });

      const handleJumps = jest.spyOn(
        movingGameObject as MovingGameObjectUnprotected,
        'updatePosition',
      );

      movingGameObject.update();

      expect(handleJumps).toBeCalled();
    });
  });

  describe('position', () => {
    it('should move horizontally when updated and has positive x-axis velocity', () => {
      const movingGameObject = new MovingGameObject({
        ...movingObjectBaseProps,
        velocity: { x: 2, y: 0 }, // <-- x-axis velocity
        posX: 10,
      });

      Reflect.defineProperty(movingGameObject, 'moveHorizontally', () => false);

      const moveHorizontally = jest.spyOn(
        movingGameObject as MovingGameObjectUnprotected,
        'moveHorizontally',
      );

      movingGameObject.update(); // <-- x-is updated

      expect(moveHorizontally).toBeCalled();
    });

    it('should increase x-axis position when updated while is moving right', () => {
      const movingGameObject = new MovingGameObject({
        ...movingObjectBaseProps,
        velocity: { x: 2, y: 0 }, // <-- positive x-axis velocity makes it moving right
        posX: 10,
      });

      expect(movingGameObject.isMovingRight).toEqual(true);

      movingGameObject.update();

      expect(movingGameObject.position.x).toEqual(12);

      movingGameObject.update();

      expect(movingGameObject.position.x).toEqual(14);
    });

    it('should decrease x-axis position when updated while is moving left', () => {
      const movingGameObject = new MovingGameObject({
        ...movingObjectBaseProps,
        velocity: { x: -2, y: 0 }, // <-- negative x-axis velocity makes it moving left
        posX: 10,
      });

      expect(movingGameObject.isMovingLeft).toEqual(true);

      movingGameObject.update();

      expect(movingGameObject.position.x).toEqual(8);

      movingGameObject.update();

      expect(movingGameObject.position.x).toEqual(6);
    });
  });

  describe('movement modifiers', () => {
    it('should add a movement modifier to the movement modifiers when registering a new one', () => {
      const movingGameObject = new MovingGameObject({
        ...movingObjectBaseProps,
      });

      // Creating the modifier
      const movementModifier = new MovementModifier({
        gameObject: movingGameObject,
        apply: (): number => 1,
      });

      // Registering the modifier
      movingGameObject.registerMovementModifier(movementModifier);

      // Expecting the modifier to be added
      expect(movingGameObject.movementModifiers).toHaveLength(1);
      expect(movingGameObject.movementModifiers[0]).toEqual(movementModifier);
    });

    it('should apply a movement modifier when updated if a movement modifier is set', () => {
      const movingGameObject = new MovingGameObject({
        ...movingObjectBaseProps,
        velocity: { x: 1, y: 0 }, // <-- positive x velocity makes object moving right
      });

      const movementModifier = new MovementModifier({
        gameObject: movingGameObject,
        apply: (): number => 1,
        modifyRight: true, // <-- apply modifier when moving right
      });

      movingGameObject.registerMovementModifier(movementModifier);

      const applyModifier = jest.spyOn(movementModifier, 'apply');

      movingGameObject.update();

      expect(applyModifier).toBeCalled();
    });
  });

  describe('jump', () => {
    it('should be jumping when a jump is made', () => {
      const { canvas, ctx } = setCanvasAndContext();

      const movingGameObject = new MovingGameObject({
        canvas,
        ctx,
        canJump: true,
      });

      expect(movingGameObject.isJumping).toEqual(false);

      movingGameObject.startAJump();

      expect(movingGameObject.isJumping).toEqual(true);
    });

    it('should move up when jumping', () => {
      const { canvas, ctx } = setCanvasAndContext();

      const movingGameObject = new MovingGameObject({
        canvas,
        ctx,
        canJump: true,
        jumpStrength: 5,
      });

      expect(movingGameObject.isJumping).toEqual(false);

      movingGameObject.startAJump();

      expect(movingGameObject.isJumping).toEqual(true);
      expect(movingGameObject.jumpElevation).toEqual(0);
      expect(movingGameObject.velocity.y).toEqual(
        -movingGameObject.jumpStrength,
      );

      movingGameObject.update();

      expect(movingGameObject.jumpElevation).toEqual(
        movingGameObject.jumpStrength,
      );
    });

    it('should keep moving up when jumping until the max elevation is not reached', () => {
      const { canvas, ctx } = setCanvasAndContext();
      const movingGameObject = new MovingGameObject({
        canvas,
        ctx,
        canJump: true,
      });

      expect(movingGameObject.isJumping).toEqual(false);

      movingGameObject.jumpStrength = 1;
      movingGameObject.maxJumpElevation = 3;

      movingGameObject.startAJump();

      expect(movingGameObject.isJumping).toEqual(true);
      expect(movingGameObject.jumpElevation).toEqual(0);
      expect(movingGameObject.velocity.y).toEqual(
        -movingGameObject.jumpStrength,
      );

      movingGameObject.update();

      expect(movingGameObject.jumpElevation).toEqual(
        movingGameObject.jumpStrength * 1,
      );

      movingGameObject.update();

      expect(movingGameObject.jumpElevation).toEqual(
        movingGameObject.jumpStrength * 2,
      );

      movingGameObject.update();
      movingGameObject.update();

      expect(movingGameObject.jumpElevation).toBeLessThan(
        movingGameObject.jumpStrength * 4,
      );
    });

    it('should be start falling when the max elevation is reached', () => {
      const { canvas, ctx } = setCanvasAndContext();
      const movingGameObject = new MovingGameObject({
        canvas,
        ctx,
        canJump: true,
      });

      expect(movingGameObject.isJumping).toEqual(false);

      movingGameObject.jumpStrength = 1;
      movingGameObject.maxJumpElevation = 3;

      movingGameObject.startAJump();
      movingGameObject.update();

      expect(movingGameObject.jumpElevation).toEqual(
        movingGameObject.jumpStrength,
      );
      expect(movingGameObject.isFalling).toEqual(false);

      movingGameObject.update(); // jumpElevation = 2
      movingGameObject.update(); // jumpElevation = 3

      expect(movingGameObject.isFalling).toEqual(true);
    });

    it('should move down when is falling', () => {
      const { canvas, ctx } = setCanvasAndContext();
      const movingGameObject = new MovingGameObject({
        canvas,
        ctx,
        canJump: true,
      });

      movingGameObject.jumpStrength = 1;
      movingGameObject.maxJumpElevation = 3;
      movingGameObject.jumpElevation = 3;

      movingGameObject.fall();

      const initialVerticalPosition = movingGameObject.position.y;

      expect(movingGameObject.isFalling).toEqual(true);
      expect(movingGameObject.velocity.y).toEqual(gravity);

      movingGameObject.update();
      expect(movingGameObject.velocity.y).toEqual(gravity * 2);
      expect(movingGameObject.position.y).toEqual(
        initialVerticalPosition + gravity,
      );

      movingGameObject.update();
      expect(movingGameObject.velocity.y).toEqual(gravity * 3);
      expect(movingGameObject.position.y).toEqual(
        initialVerticalPosition + gravity * 3,
      );

      movingGameObject.update();
      expect(movingGameObject.velocity.y).toEqual(gravity * 4);
      expect(movingGameObject.position.y).toEqual(
        initialVerticalPosition + gravity * 6,
      );

      movingGameObject.update();
      expect(movingGameObject.velocity.y).toEqual(gravity * 5);
      expect(movingGameObject.position.y).toEqual(
        initialVerticalPosition + gravity * 10,
      );
    });

    it('should move down when is falling and jump is not ended', () => {
      const { canvas, ctx } = setCanvasAndContext();
      const movingGameObject = new MovingGameObject({
        canvas,
        ctx,
        canJump: true,
      });

      movingGameObject.jumpStrength = 10;
      movingGameObject.maxJumpElevation = 30;

      movingGameObject.startAJump();
      movingGameObject.update();
      movingGameObject.update();

      expect(movingGameObject.isFalling).toEqual(false); // Not yet reached max elevation

      movingGameObject.update();

      expect(movingGameObject.isFalling).toEqual(true); // Reached max elevation, start falling

      const initialVerticalPosition = movingGameObject.position.y;

      expect(movingGameObject.velocity.y).toEqual(gravity);

      movingGameObject.update();
      expect(movingGameObject.velocity.y).toEqual(gravity * 2);
      expect(movingGameObject.position.y).toEqual(
        initialVerticalPosition + gravity,
      );

      movingGameObject.update();
      expect(movingGameObject.velocity.y).toEqual(gravity * 3);
      expect(movingGameObject.position.y).toEqual(
        initialVerticalPosition + gravity * 3,
      );

      movingGameObject.update();
      expect(movingGameObject.velocity.y).toEqual(gravity * 4);
      expect(movingGameObject.position.y).toEqual(
        initialVerticalPosition + gravity * 6,
      );
    });
  });
});
