import Player from '@/classes/entities/Player';
import { mount, wrapper } from '../../helpers/setup';
import { gravity, keyMap } from '@/libs/constants';

const createCanvas = () => {
  const canvas = document.createElement('canvas') as HTMLCanvasElement;
  canvas.setAttribute('id', 'test-canvas');
  const ctx = canvas.getContext('2d') as CanvasRenderingContext2D;

  return { canvas, ctx };
};

describe('Player', () => {
  beforeEach(() => {
    mount();
  });

  it('should have a 0 default velocity both on X and Y axis', () => {
    const { canvas, ctx } = createCanvas();
    const player = new Player({ canvas, ctx });
    expect(player.velocity.y).toEqual(0);
    expect(player.velocity.x).toEqual(0);
  });

  it('should not increase the Y position when player is updated the first time', function () {
    const { canvas, ctx } = createCanvas();
    const player = new Player({ canvas, ctx, velY: 0 });
    const originalYPosition = player.position.y;
    const originalYVelocity = player.velocity.y;
    let increasedVelocity = 0;
    let updatedPosition = 0;

    player.update([]);
    updatedPosition = originalYPosition + originalYVelocity;
    expect(player.position.y).toEqual(updatedPosition);

    player.update([]);
    increasedVelocity += gravity;
    updatedPosition += increasedVelocity;
    expect(player.position.y).toEqual(updatedPosition);

    player.update([]);
    increasedVelocity += gravity;
    updatedPosition += increasedVelocity;
    expect(player.position.y).toEqual(updatedPosition);
  });

  it('should increase the Y position by the gravity value  when player is updated the second time', function () {
    const { canvas, ctx } = createCanvas();
    const player = new Player({ canvas, ctx });
    const originalYPosition = player.position.y;

    player.update([]);
    player.update([]);

    expect(player.position.y).toEqual(originalYPosition + gravity);
  });

  it('should stick to the bottom if the player bottom position plus its velocity is over the canvas height', function () {
    const { canvas, ctx } = createCanvas();
    const player = new Player({ canvas, ctx });

    player.velocity.y = 20;
    player.position.y = canvas.height;

    player.update([]);

    expect(player.position.y).toEqual(canvas.height - player.height);
  });

  it('should be falling when the player bottom position is lower than the canvas height', () => {
    const { canvas, ctx } = createCanvas();
    const player = new Player({ posY: canvas.height - 100, canvas, ctx });

    expect(player.isFalling).toBeTruthy();
  });

  it('should not be falling when the player bottom position is equal than the canvas height', () => {
    const { canvas, ctx } = createCanvas();
    const player = new Player({ posY: canvas.height, canvas, ctx });

    expect(player.isFalling).not.toBeTruthy();
  });

  it('should not be falling when the player bottom position is higher than the canvas height', () => {
    const { canvas, ctx } = createCanvas();
    const player = new Player({ posY: canvas.height + 10, canvas, ctx });

    expect(player.isFalling).not.toBeTruthy();
  });

  describe('MovementsController', () => {
    it('should attempt a jump when the UP button is pressed', () => {
      const { canvas, ctx } = createCanvas();
      const player = new Player({ canvas, ctx });
      const keydownEvent = new KeyboardEvent('keydown', { key: keyMap.up });
      const attemptJump = jest.spyOn(player, 'attemptJump');

      dispatchEvent(keydownEvent);

      expect(attemptJump).toBeCalled();
    });

    it('should jump when attempting a jump while is not falling and not jumping', () => {
      const { canvas, ctx } = createCanvas();
      const player = new Player({ canvas, ctx });

      player.isJumping = false;
      player.isFalling = false;
      player.keys.up.pressed = false;

      const jump = jest.spyOn(Player.prototype, 'startAJump');

      player.attemptJump();

      expect(jump).toBeCalled();
    });

    it('should not jump when the UP button is pressed if is already jumping', () => {
      const { canvas, ctx } = createCanvas();
      const player = new Player({ canvas, ctx });
      player.isJumping = true;
      player.keys.up.pressed = false;
      const keydownEvent = new KeyboardEvent('keydown', { key: keyMap.up });
      const attemptJump = jest.spyOn(Player.prototype, 'attemptJump');
      const jump = jest.spyOn(Player.prototype, 'startAJump');

      dispatchEvent(keydownEvent);

      expect(attemptJump).toBeCalled();
      expect(player.isFalling).toBeTruthy();
    });

    it('should move right when the RIGHT button is pressed', () => {
      const { canvas, ctx } = createCanvas();
      const player = new Player({ canvas, ctx });
      const keydownEvent = new KeyboardEvent('keydown', { key: keyMap.right });
      const moveRight = jest.spyOn(player, 'moveRight');

      expect(player.keys.right.pressed).toEqual(false);

      dispatchEvent(keydownEvent);

      expect(player.keys.right.pressed).toEqual(true);
      expect(moveRight).toBeCalled();
    });

    it('should move left when the LEFT button is pressed', () => {
      const { canvas, ctx } = createCanvas();
      const player = new Player({ canvas, ctx });
      const keydownEvent = new KeyboardEvent('keydown', { key: keyMap.left });
      const moveRight = jest.spyOn(player, 'moveLeft');

      expect(player.keys.left.pressed).toEqual(false);

      dispatchEvent(keydownEvent);

      expect(player.keys.left.pressed).toEqual(true);
      expect(moveRight).toBeCalled();
    });

    it('should stop moving right when the RIGHT button is released', () => {
      const { canvas, ctx } = createCanvas();
      const player = new Player({ canvas, ctx });

      new KeyboardEvent('keydown', { key: keyMap.right });

      expect(player.keys.right.pressed).toEqual(true);

      const keyupEvent = new KeyboardEvent('keyup', { key: keyMap.right });

      const stopMovingRight = jest.spyOn(player, 'stopMovingRight');

      dispatchEvent(keyupEvent);

      expect(player.keys.right.pressed).toEqual(false);

      expect(stopMovingRight).toBeCalled();
    });

    it('should stop moving left when the LEFT button is released', () => {
      const { canvas, ctx } = createCanvas();
      const player = new Player({ canvas, ctx });

      new KeyboardEvent('keydown', { key: keyMap.left });

      expect(player.keys.left.pressed).toEqual(true);

      const keyupEvent = new KeyboardEvent('keyup', { key: keyMap.left });

      const stopMovingLeft = jest.spyOn(player, 'stopMovingLeft');

      dispatchEvent(keyupEvent);

      expect(player.keys.left.pressed).toEqual(false);

      expect(stopMovingLeft).toBeCalled();
    });
  });
});
