import CollisionController from '@/classes/controllers/CollisionController';
import { instanciateGameObject } from '../../helpers';

describe('CollisionController', () => {
  it('should detect obstacles on the right of an object when some are present', () => {
    const { gameObject: object1 } = instanciateGameObject({
      posX: 10,
      width: 20,
    });
    const { gameObject: object2 } = instanciateGameObject({
      posX: 30,
      isObstacle: { left: true },
    });

    const collisionController = new CollisionController({
      stageObjects: [object1, object2],
    });

    const detect = collisionController.detectFor(object1);

    const hasObstaclesOnTheRight = detect.obstaclesOnTheRight();

    expect(hasObstaclesOnTheRight).toEqual(true);
  });

  it('should not detect obstacles on the right of an object when none are present', () => {
    const { gameObject: object1 } = instanciateGameObject({
      posX: 10,
      width: 20,
    });

    const { gameObject: object2 } = instanciateGameObject({
      posX: 41,
      isObstacle: { left: true },
    });

    const collisionController = new CollisionController({
      stageObjects: [object1, object2],
    });

    const detect = collisionController.detectFor(object1);

    const hasObstaclesOnTheRight = detect.obstaclesOnTheRight();

    expect(hasObstaclesOnTheRight).toEqual(false);
  });
});
