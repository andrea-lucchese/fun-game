import MovingGameObject from '@/classes/entities/MovingGameObject';
import { MovementModifier } from '@/classes/modifiers/MovementModifier';

const setCanvasAndContext = () => {
  const canvas = document.createElement('canvas');
  const ctx = canvas.getContext('2d') as CanvasRenderingContext2D;
  return { canvas, ctx };
};

const movingObjectBaseProps = setCanvasAndContext();

const resetMovingObjectBaseProps = () => {
  const { canvas, ctx } = setCanvasAndContext();
  movingObjectBaseProps.canvas = canvas;
  movingObjectBaseProps.ctx = ctx;
};

describe('MovementModifier', () => {
  beforeEach(() => {
    resetMovingObjectBaseProps();
  });

  it('should sum the value of a modifier when a sum modifier is set to modify right and the object is moving right', () => {
    const movingGameObject = new MovingGameObject({
      ...movingObjectBaseProps,
      velocity: { x: 1, y: 0 },
      posX: 0,
      posY: 0,
    });

    const movementModifier: MovementModifier = new MovementModifier({
      gameObject: movingGameObject,
      apply: (): number => 1,
      modifyRight: true,
      type: MovementModifier.TYPE_SUM as MovementModifier['type'],
    });

    movingGameObject.registerMovementModifier(movementModifier);

    movingGameObject.update();

    expect(movingGameObject.isMovingRight).toEqual(true);
    expect(movingGameObject.position.x).toEqual(2);
  });

  it('should not sum the value of a modifier when a sum modifier is not set to modify right and the object is moving right', () => {
    const movingGameObject = new MovingGameObject({
      ...movingObjectBaseProps,
      velocity: { x: 1, y: 0 },
      posX: 0,
      posY: 0,
    });

    const movementModifier: MovementModifier = new MovementModifier({
      gameObject: movingGameObject,
      apply: (): number => 1,
      modifyRight: false,
      type: MovementModifier.TYPE_SUM as MovementModifier['type'],
    });

    movingGameObject.registerMovementModifier(movementModifier);

    movingGameObject.update();

    expect(movingGameObject.isMovingLeft).toEqual(false);
    expect(movingGameObject.isMovingRight).toEqual(true);
    expect(movingGameObject.position.x).toEqual(1);
  });

  it('should sum the value of a modifier when a sum modifier is set to modify left and the object is moving left', () => {
    const movingGameObject = new MovingGameObject({
      ...movingObjectBaseProps,
      velocity: { x: -1, y: 0 },
      posX: 0,
      posY: 0,
    });

    const movementModifier: MovementModifier = new MovementModifier({
      gameObject: movingGameObject,
      apply: (): number => -1,
      modifyLeft: true,
      type: MovementModifier.TYPE_SUM as MovementModifier['type'],
    });

    movingGameObject.registerMovementModifier(movementModifier);

    movingGameObject.update();

    expect(movingGameObject.isMovingLeft).toEqual(true);
    expect(movingGameObject.position.x).toEqual(-2);
  });

  it('should not sum the value of a modifier when a sum modifier is not set to modify left and the object is moving left', () => {
    const movingGameObject = new MovingGameObject({
      ...movingObjectBaseProps,
      velocity: { x: -1, y: 0 },
      posX: 0,
    });

    const movementModifier: MovementModifier = new MovementModifier({
      gameObject: movingGameObject,
      apply: (): number => -1,
      modifyLeft: false,
      type: MovementModifier.TYPE_SUM as MovementModifier['type'],
    });

    movingGameObject.registerMovementModifier(movementModifier);

    movingGameObject.update();

    expect(movingGameObject.isMovingLeft).toEqual(true);
    expect(movingGameObject.position.x).toEqual(-1);
  });
});
