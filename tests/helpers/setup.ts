import * as fs from 'fs';
import path from 'path';

const innerHtml = fs.readFileSync(
  path.resolve(__dirname, '../../public/index.html'),
  'utf8',
);

jest.dontMock('fs');

// const aaa = { ...document };
export const wrapper = document.body;

export const mount = () => {
  document.body.innerHTML = innerHtml;
  return document.getElementById('root') as HTMLDivElement;

  // return wrapper.root;
};
