import { GameObjectProps } from '@/ts/classes';
import GameObject from '@/classes/entities/GameObject';

export const instanciateGameObject = (props: Partial<GameObjectProps>) => {
  const canvas = document.createElement('canvas');
  const ctx = canvas.getContext('2d') as CanvasRenderingContext2D;
  props.posX = props.posX || 0;
  props.posY = props.posY || 0;
  const gameObject = new GameObject({ canvas, ctx, ...props });

  return { canvas, ctx, gameObject };
};
