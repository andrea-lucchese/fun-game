import { mount, wrapper } from './helpers/setup';
import marioGame from '@/mario-game';

describe('Mario game', () => {
  beforeEach(() => {
    mount();
    marioGame();
  });

  it('should render a canvas element', function () {
    const canvas = document.querySelector('canvas');

    wrapper.appendChild(document.createElement('p'));

    expect(canvas).toBeTruthy();
  });
});
