import Player from '@/classes/entities/Player';
import GameObject from '@/classes/entities/GameObject';
import GameStage from '@/classes/entities/GameStage';

/**
 * Creates the canvas element and appends it to the dom.
 * @return {{
 *     canvas: HTMLCanvasElement,
 *     ctx: CanvasRenderingContext2D
 * }}
 */
const setCanvas = (): {
  canvas: HTMLCanvasElement;
  ctx: CanvasRenderingContext2D;
} => {
  const canvas = document.createElement('canvas');

  canvas.width = innerWidth;
  canvas.height = innerHeight;

  const root = document.getElementById('root');
  const ctx = canvas.getContext('2d') as CanvasRenderingContext2D;

  if (root) root.append(canvas);

  return { canvas, ctx };
};

const marioGame = (): void => {
  const { canvas, ctx } = setCanvas();

  const platforms = [
    new GameObject({
      canvas,
      ctx,
      posX: 200,
      posY: 200,
      width: 100,
      height: 30,
      color: 'blue',
    }),

    new GameObject({
      canvas,
      ctx,
      posX: 200,
      posY: 100,
      width: 100,
      height: 30,
      color: 'blue',
    }),
  ];

  const player = new Player({ canvas, ctx, platforms });
  const stage = new GameStage({ canvas, ctx, player, platforms });

  stage.animate();
};

export default marioGame;
