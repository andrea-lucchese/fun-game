import { GameObjectProps } from './GameObjectProps';

export interface MovingGameObjectProps extends GameObjectProps {
  velocity?: { x: number; y: number };
  isFalling?: boolean;
  isJumping?: boolean;
  jumpElevation?: number;
  jumpStrength?: number;
  maxJumpElevation?: number;
  acceleration?: number;
  maxSpeed?: number;
  canJump?: boolean;
}
