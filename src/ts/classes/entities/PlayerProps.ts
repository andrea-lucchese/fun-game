import { MovingGameObjectProps } from '@/ts/classes';
import GameObject from '@/classes/entities/GameObject';

export interface PlayerProps extends MovingGameObjectProps {
  velX?: number;
  velY?: number;
  jumpStrength?: number;
  platforms?: GameObject[];
}
