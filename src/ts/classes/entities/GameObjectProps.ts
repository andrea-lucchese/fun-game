import { Obstacle } from './Obstacle';

export interface GameObjectProps {
  canvas: HTMLCanvasElement;
  ctx: CanvasRenderingContext2D;
  posX?: number;
  posY?: number;
  width?: number;
  height?: number;
  color?: string;
  isObstacle?: Partial<Obstacle>;
}
