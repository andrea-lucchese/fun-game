export interface Obstacle {
  top?: boolean;
  right?: boolean;
  bottom?: boolean;
  left?: boolean;
}
