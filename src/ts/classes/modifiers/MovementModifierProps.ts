import MovingGameObject from '@/classes/entities/MovingGameObject';

export interface MovementModifierProps {
  gameObject: MovingGameObject;
  apply: (movingGameObject?: MovingGameObject) => number;
  modifyUp?: boolean;
  modifyRight?: boolean;
  modifyDown?: boolean;
  modifyLeft?: boolean;
  type?: 'sum' | 'subtract' | 'override';
  next?: boolean;
}
