export const playerDefaultProps = {
  x: 0,
  y: 0,
  position: {
    x: 0,
    y: 0,
  },
  velocity: {
    x: 0,
    y: 0,
  },
  width: 30,
  height: 30,
  jumpHeight: 50,
  maxJumpElevation: 200,
  acceleration: 2,
  maxSpeed: 20,
  keys: {
    left: { pressed: false },
    right: { pressed: false },
    up: { pressed: false },
    down: { pressed: false },
  },
};
