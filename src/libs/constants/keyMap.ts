export const keyMap = {
  up: 'w',
  down: 's',
  left: 'a',
  right: 'd',
};
