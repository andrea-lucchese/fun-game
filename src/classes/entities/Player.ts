import { keyMap, playerDefaultProps } from '@/libs/constants';
import { PlayerProps } from '@/ts/classes';
import GameObject from '@/classes/entities/GameObject';
import MovingGameObject from '@/classes/entities/MovingGameObject';

type KeyStatus = { pressed: boolean };

class Player extends MovingGameObject {
  platforms: GameObject[];
  keys: {
    left: KeyStatus;
    right: KeyStatus;
    up: KeyStatus;
    down: KeyStatus;
  };

  constructor(props: PlayerProps) {
    const { platforms, velX, velY, jumpStrength } = props;

    super(props);

    this.velocity = {
      x: velX || playerDefaultProps.velocity.x,
      y: velY || playerDefaultProps.velocity.y,
    };
    this.jumpStrength = jumpStrength || playerDefaultProps.jumpHeight;
    this.maxJumpElevation = playerDefaultProps.maxJumpElevation;
    this.acceleration = playerDefaultProps.acceleration;
    this.maxSpeed = playerDefaultProps.maxSpeed;

    this.keys = playerDefaultProps.keys; // MovementController
    this.platforms = platforms || []; // CollisionController

    this.initKeyListeners(); // MovementController
  }

  //#region MovementController
  public move(direction: 'left' | 'right'): void {
    this.keys[direction].pressed = true;
  }

  public stopMoving(direction: 'left' | 'right'): void {
    this.keys[direction].pressed = false;
  }

  public moveRight(): void {
    this.move('right');
  }

  public stopMovingRight(): void {
    this.stopMoving('right');
  }

  public moveLeft(): void {
    this.move('left');
  }

  public stopMovingLeft(): void {
    this.stopMoving('left');
  }

  private initKeyListeners = () => {
    addEventListener('keydown', ({ key }) => {
      const { up, left, right } = keyMap;

      switch (key) {
        case up:
          this.attemptJump();
          break;
        case right:
          this.moveRight();
          break;
        case left:
          this.moveLeft();
          break;
      }
    });

    addEventListener('keyup', ({ key }) => {
      const { up, left, right } = keyMap;

      switch (key) {
        case up:
          this.keys.up.pressed = false;
          break;
        case right:
          this.stopMovingRight();
          break;
        case left:
          this.stopMovingLeft();
          break;
      }
    });
  };

  public attemptJump(): void {
    if (this.isFalling || this.isJumping || this.keys.up.pressed) return;
    this.keys.up.pressed = true;
    this.startAJump();
  }

  private handleMovements = () => {
    if (this.keys.right.pressed) this.accelerateRight();
    if (!this.keys.right.pressed) this.decelerateRight();
    if (this.keys.left.pressed) this.accelerateLeft();
    if (!this.keys.left.pressed) this.decelerateLeft();
  };
  //#endregion MovementController

  public update(platforms: GameObject[]): void {
    super.update([]);

    this.handleMovements();
  }
}

export default Player;
