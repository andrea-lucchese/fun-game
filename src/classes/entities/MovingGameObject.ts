import { MovementModifier } from '@/classes/modifiers/MovementModifier';
import { MovingGameObjectProps } from '@/ts/classes';
import { gravity } from '@/libs/constants/gravity';
import GameObject from '@/classes/entities/GameObject';

class MovingGameObject extends GameObject {
  velocity: { x: number; y: number };
  isFalling: boolean;
  isJumping: boolean;
  jumpElevation: number;
  jumpStrength: number;
  maxJumpElevation: number;
  acceleration: number;
  maxSpeed: number;
  movementModifiers: MovementModifier[];
  canJump: boolean;

  constructor(props: MovingGameObjectProps) {
    const { canvas } = props;

    super(props);

    const {
      velocity,
      isJumping,
      jumpElevation,
      jumpStrength,
      maxJumpElevation,
      acceleration,
      maxSpeed,
      canJump,
    } = props;

    this.velocity = { x: velocity?.x || 0, y: velocity?.x || 0 };
    this.isFalling = this.position.y + this.height < canvas.height;
    this.isJumping = isJumping || false;
    this.jumpElevation = jumpElevation || 0;
    this.jumpStrength = jumpStrength || 5;
    this.maxJumpElevation = maxJumpElevation || 10;
    this.acceleration = acceleration || 0;
    this.maxSpeed = maxSpeed || 0;
    this.movementModifiers = [];
    this.canJump = canJump !== undefined ? canJump : true;
  }

  //#region Getters
  public get isMovingRight(): boolean {
    return this.velocity.x > 0;
  }

  public get isMovingLeft(): boolean {
    return this.velocity.x < 0;
  }

  public get isMovingHorizontally(): boolean {
    return this.isMovingRight || this.isMovingLeft;
  }

  public get isMovingUp(): boolean {
    return this.velocity.y < 0;
  }

  public get isMovingDown(): boolean {
    return this.velocity.y > 0;
  }

  public get isMovingVertically(): boolean {
    return this.isMovingUp || this.isMovingDown;
  }
  //#endregion Getters

  //#region Movement Modifiers
  public registerMovementModifier = (
    movementModifier: MovementModifier,
  ): void => {
    this.movementModifiers.push(movementModifier);
  };

  private applyHorizontalMovementModifiers = () => {
    return this.applyMovementModifiers('x');
  };

  private applyVerticalMovementModifiers = () => {
    return this.applyMovementModifiers('y');
  };

  private applyMovementModifiers = (axis: 'x' | 'y'): number => {
    let result = 0;
    this.movementModifiers.every((modifier) => {
      if (axis === 'x' && !modifier.shouldApplyHorizontally())
        return modifier.next;

      if (axis === 'y' && !modifier.shouldApplyVertically())
        return modifier.next;

      switch (modifier.type) {
        case MovementModifier.TYPE_SUM:
        default:
          result += modifier.apply();
          break;
        case MovementModifier.TYPE_OVERRIDE:
          result = modifier.apply();
          break;
      }

      return modifier.next;
    });

    return result;
  };
  //#endregion Movement Modifiers

  //#region Accelerators
  public accelerateRight(): void {
    if (this.velocity.x < this.maxSpeed) this.velocity.x += this.acceleration;
  }

  public decelerateRight(): void {
    if (this.velocity.x > 0) this.velocity.x -= this.acceleration;
  }

  public accelerateLeft(): void {
    if (this.velocity.x > -this.maxSpeed) this.velocity.x -= this.acceleration;
  }

  public decelerateLeft(): void {
    if (this.velocity.x < 0) this.velocity.x += this.acceleration;
  }
  //#endregion Accelerators

  //#region Movements
  protected moveHorizontally = (): void => {
    const modifier = this.applyHorizontalMovementModifiers();
    this.position.x += modifier + this.velocity.x;
  };

  protected moveVertically = (): void => {
    const modifier = this.applyVerticalMovementModifiers();
    this.position.y += modifier + this.velocity.y;
  };

  protected updatePosition = (): void => {
    this.moveHorizontally();
    this.moveVertically();
  };
  //#endregion Accelerators

  //#region Collision Controller
  private interactWithPlatforms = (platforms: GameObject[] = []) => {
    let highestCollidingPlatformPosition = this.canvas.height;

    platforms.forEach((platform) => {
      const isToCollideVerticallyWithPlatform =
        this.bottomMarginPosition === platform.topMarginPosition ||
        platform.topMarginPosition + this.bottomMarginPosition <= 10 ||
        this.bottomMarginPosition + platform.topMarginPosition <= 10;

      const playerCollidesHorizontallyWithPlatform =
        this.position.x + this.width >= platform.position.x &&
        this.position.x <= platform.position.x + platform.width;

      const playerIsStandingOnAPlatform =
        this.bottomMarginPosition === platform.position.y &&
        this.velocity.y === 0 &&
        this.isFalling === false;
      if (
        playerIsStandingOnAPlatform &&
        !playerCollidesHorizontallyWithPlatform
      ) {
        this.fall();
        return;
      }

      if (
        this.velocity.y > 0 &&
        this.isFalling &&
        isToCollideVerticallyWithPlatform &&
        playerCollidesHorizontallyWithPlatform
      ) {
        if (platform.topMarginPosition < highestCollidingPlatformPosition) {
          highestCollidingPlatformPosition = platform.topMarginPosition;
        }
      }
    });

    if (highestCollidingPlatformPosition === this.canvas.height) return false;
    this.stopFalling(highestCollidingPlatformPosition);

    return true;
  };

  public hasReachedTheBottom = (): boolean => {
    const playerBottomPosition = this.position.y + this.height;
    return playerBottomPosition > this.canvas.height;
  };

  public interactWithCanvas = (): void => {
    if (!this.isJumping && this.isFalling && !this.hasReachedTheBottom()) {
      this.fall();
    } else if (this.hasReachedTheBottom()) {
      this.stopFalling(this.canvas.height - this.height);
      this.stopJumping();
    }
  };
  //#endregion Collision Controller

  //#region Falling
  public fall(): void {
    if (!this.isFalling) {
      this.velocity.y = 0;
      this.isFalling = true;
    }

    this.velocity.y += gravity;

    if (this.jumpElevation > 0) {
      this.jumpElevation -= gravity;
    } else {
      this.isJumping = false;
    }
  }

  public stopFalling(restPosition: number): void {
    if (this.isFalling) this.isFalling = false;
    this.jumpElevation = 0;
    this.velocity.y = 0;
    this.position.y = restPosition;

    this.stopJumping();
  }
  //#endregion Falling

  //#region Jumping
  public startAJump(): void {
    this.isJumping = true;
    this.isFalling = false;
    this.jumpElevation = 0;
    this.velocity.y -= this.jumpStrength;
  }

  public keepJumpingUp = (): void => {
    this.jumpElevation -= this.velocity.y;
  };

  public stopJumping(): void {
    this.isJumping = false;
  }

  public isJumpingDown(): boolean {
    return this.isJumping && this.isFalling;
  }

  public isJumpingUp = (): boolean => this.isJumping && !this.isFalling;

  public jumpReachedMaxElevation = (): boolean =>
    this.isJumping && this.jumpElevation >= this.maxJumpElevation;

  private handleJumps = () => {
    if (this.isJumpingUp()) this.keepJumpingUp();
    if (this.jumpReachedMaxElevation() || this.isJumpingDown()) this.fall();
  };
  //#endregion Jumps

  public update(platforms: GameObject[] = []): void {
    this.draw();
    this.updatePosition();

    if (this.canJump) this.handleJumps();

    this.interactWithCanvas();
  }
}

export default MovingGameObject;
