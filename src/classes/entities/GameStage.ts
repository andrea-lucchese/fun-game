import GameObject from '@/classes/entities/GameObject';
import Player from '@/classes/entities/Player';

interface StageProps {
  canvas: HTMLCanvasElement;
  ctx: CanvasRenderingContext2D;
  player: Player;
  platforms: GameObject[];
}

class GameStage {
  canvas: HTMLCanvasElement;
  ctx: CanvasRenderingContext2D;
  player: Player;
  platforms: GameObject[];

  constructor({ ctx, canvas, player, platforms }: StageProps) {
    this.canvas = canvas;
    this.ctx = ctx;
    this.player = player;
    this.platforms = platforms;
  }

  updatePlatforms = (): void => {
    this.platforms.forEach((platform) => {
      platform.draw();
    });
  };

  animate = (): void => {
    requestAnimationFrame(this.animate);
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

    this.updatePlatforms();
    this.player.update(this.platforms);
  };
}

export default GameStage;
