import { GameObjectProps, Obstacle } from '@/ts/classes';

/**
 * A low-level object, having with, height, position
 * @Injectable
 */
class GameObject {
  canvas: HTMLCanvasElement;
  ctx: CanvasRenderingContext2D;
  position: { x: number; y: number };
  width: number;
  height: number;
  color: string;
  isObstacle?: Partial<Obstacle>;

  constructor({
    canvas,
    ctx,
    posX,
    posY,
    width,
    height,
    color,
    isObstacle,
  }: GameObjectProps) {
    this.canvas = canvas;
    this.ctx = ctx;
    this.position = {
      x: posX || 0,
      y: posY || 0,
    };
    this.width = width || 30;
    this.height = height || 30;
    this.color = color || 'red';
    this.isObstacle = {
      top: isObstacle?.top || false,
      right: isObstacle?.right || false,
      bottom: isObstacle?.bottom || false,
      left: isObstacle?.left || false,
    };
  }

  public draw(): void {
    const { x, y } = this.position;
    const { width, height } = this;

    this.ctx.fillStyle = this.color || 'red';
    this.ctx.fillRect(x, y, width, height);
  }

  /**
   * Gets the y-axis position of the object's top margin.
   */
  public get topMarginPosition(): number {
    return this.position.y;
  }

  /**
   * Gets the y-axis position of the object's bottom margin.
   */
  public get bottomMarginPosition(): number {
    return this.position.y + this.height;
  }

  /**
   * Gets the x-axis position of the object's right margin.
   */
  public get rightMarginPosition(): number {
    return this.position.x + this.width;
  }

  /**
   * Gets the x-axis position of the object's left margin.
   */
  public get leftMarginPosition(): number {
    return this.position.x;
  }

  /**
   * Determines if the top margin touches but not intercept another object's bottom margin.
   * @param gameObject
   */
  public touchesAbove = (gameObject: GameObject): boolean => {
    return this.topMarginPosition === gameObject.bottomMarginPosition;
  };

  /**
   * Determines if the bottom margin touches but not intercept another object's top margin.
   * @param gameObject
   */
  public touchesBelow = (gameObject: GameObject): boolean => {
    return this.bottomMarginPosition === gameObject.topMarginPosition;
  };

  public touchesOnTheRight = (gameObject: GameObject): boolean => {
    return this.rightMarginPosition === gameObject.leftMarginPosition;
  };

  public touchesOnTheLeft = (gameObject: GameObject): boolean => {
    return this.leftMarginPosition === gameObject.rightMarginPosition;
  };
}

export default GameObject;
