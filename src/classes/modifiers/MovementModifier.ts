import MovingGameObject from '@/classes/entities/MovingGameObject';
import { MovementModifierProps } from '@/ts/classes';

export class MovementModifier {
  gameObject: MovingGameObject;
  apply: (movingGameObject?: MovingGameObject) => number;
  modifyUp?: boolean;
  modifyRight?: boolean;
  modifyDown?: boolean;
  modifyLeft?: boolean;
  type: 'sum' | 'subtract' | 'override';
  next?: boolean;

  constructor({
    gameObject,
    apply,
    modifyUp,
    modifyRight,
    modifyDown,
    modifyLeft,
    type,
    next,
  }: MovementModifierProps) {
    this.gameObject = gameObject;
    this.apply = apply;
    this.type = type || (MovementModifier.TYPE_SUM as MovementModifier['type']);
    this.modifyUp = modifyUp !== undefined || false;
    this.modifyRight = modifyRight !== undefined ? modifyRight : true;
    this.modifyDown = modifyDown !== undefined ? modifyDown : false;
    this.modifyLeft = modifyLeft !== undefined ? modifyLeft : true;
    this.next = next !== undefined ? next : true;
  }

  public static TYPE_SUM = 'sum';
  public static TYPE_OVERRIDE = 'override';

  public shouldApplyHorizontally = (): boolean => {
    if (this.modifyRight && this.gameObject.isMovingRight) return true;
    if (this.modifyLeft && this.gameObject.isMovingLeft) return true;

    return false;
  };

  public shouldApplyVertically = (): boolean => {
    if (this.modifyUp && this.gameObject.isMovingUp) return true;
    if (this.modifyDown && this.gameObject.isMovingDown) return true;

    return false;
  };
}
