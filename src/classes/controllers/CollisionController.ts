import GameObject from '@/classes/entities/GameObject';

interface CollisionControllerProps {
  stageObjects: GameObject[];
}

class CollisionController {
  public stageObjects: GameObject[] = [];

  constructor({ stageObjects }: CollisionControllerProps) {
    this.stageObjects = stageObjects;
  }

  private detectObstaclesOnTheRight = (focusedObject: GameObject): boolean =>
    this.stageObjects.some(
      (stageObject) =>
        stageObject.isObstacle?.left &&
        focusedObject.touchesOnTheRight(stageObject),
    );

  private detectObstaclesOnTheLeft = (focusedObject: GameObject): boolean =>
    this.stageObjects.some(
      (stageObject) =>
        stageObject.isObstacle?.right &&
        focusedObject.touchesOnTheLeft(stageObject),
    );

  public detectFor = (focusedObject: GameObject) => {
    const obstaclesOnTheRight = () =>
      this.detectObstaclesOnTheRight(focusedObject);

    const obstaclesOnTheLeft = () =>
      this.detectObstaclesOnTheLeft(focusedObject);

    return { obstaclesOnTheRight, obstaclesOnTheLeft };
  };
}

export default CollisionController;
