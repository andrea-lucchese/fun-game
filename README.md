# Mario Game: a lonely sunday project

## Introduction
This code is the result of a lazy sunday at home without my family: a rare occasion to revive childhood memories of days spent typing games in Basic on my [Spectrum Sinclair 48k](https://en.wikipedia.org/wiki/ZX_Spectrum).
**The game is not finished**, but it just consists of a moving square in a Canvas that lands on platforms (this is also to better tune). 
I might continue one of these weekend to finish the movements and collision dynamics and add some graphics to it.

While it has been written following a green-red-refactor TDD cycle not much attention has been paid to its architecture.  I have been trying OOP patterns without worrying too much if it was the right thing to do.

In general the game entrypoint is the [mario-game.ts](./src/mario-game.ts) file which set the game stage on a canvas and sets a player which can move and jump on platforms. A shallow class structure is contained within the [classes](./src/classes) folder, where the game entities and dynamics are defined.

Platforms at the moment are required by the GamePlayer class, but they should rather be sole responsibility of the  GameStage.

The general idea of the [MovingGameObject](./src/classes/entities/MovingGameObject.ts) is that they have some basic movements common to all the objects with movement capabilities within the game, but these can be configurable through [MovementModifiers](./src/classes/modifiers/MovementModifier.ts).



## Configuration

You may change the configuration for Webpack within the [webpack](webpack) folder.

## Setup

### Install dependencies

Run:

```sh
  yarn install
```

## Development

### Server

Run:

```sh
  yarn serve
```

This will create a server at `http://localhost:9000/` or at the port number specified in the [webpack/configuration/config.js](webpack/configuration/config.js) file.

Automatically reloads after each file change.

### Production build

Run:

```sh
  yarn build
```

Will output all build files into the `dist` folder.

## Testing (Jest)

Run:

```sh
  yarn test
```

or watch files

```sh
  yarn test:watch
```

## Linting

### All files

Run:

```sh
  yarn lint:all
```

To fix all possible errors automatically run:

```sh
  yarn lint:all:fix
```

### TypeScript (tsc)

Run:

```sh
  yarn lint:check-types
```

There is no automatic fix option for TypeScript.

### TypeScript and JavaScript (ESLint)

Run:

```sh
  yarn lint:scripts
```

To fix all possible errors automatically run:

```sh
  yarn lint:scripts:fix
```

### Styles (StyleLint)

Run:

```sh
  yarn lint:styles
```

To fix all possible errors automatically run:

```sh
  yarn lint:styles:fix
```

## Check bundle size

Run:

```sh
  yarn check-size
```

This will create a server at `http://localhost:8888/` or at the port number specified using the `-p or --port` option via the `cli`.
